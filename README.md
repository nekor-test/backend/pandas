# Тестовое задание: импорт поопорной ведомости.

* Язык: python;
* Уровень: middle;
* Технологии: Django, DRF, Pandas, PostgreSQL.


## Описание

Требуется прочитать и разобрать поопорную ведомость, сохранить данные опор в БД и вывести их с помощью Django Rest Framework как в виде списка, так и по отдельности, передавая идентификатор.


## Сценарий

1. пользователь загружает один или несколько XLSX-файлов, содержащие данные опор;
1. пользователь формирует координаты области (прямоугольник), охватывающей часть опор;
1. пользователь запрашивает REST API по адресу `/pylons/?bbox=<КООРДИНАТЫ_ОБЛАСТИ>` и видит список загруженных опор, каждая из которых содержит атрибуты: номер опоры, ее тип и координаты;
1. пользователь запрашивает опору по идентификатору и видит данные конкретной опоры (номер, название, координаты и идентификаторы соседних опор).


## Обязательные критерии приемки

1. программа загружает и разбирает приложенные XSLX-файлы, сохраняя их данные в БД;
1. программа реализует собственное API на базе DRF;
1. программа обеспечивает поиск данных по координатам.


## Будет существенным плюсом

* наличие модульных тестов (`unit-test`);
* аккуратно оформленная поставка готового решения.


## Контроль качества исходного кода

Проект содержит в себе настроенный на `git-hooks` статический анализатор кода `flake8`, который не позволяет коммитить в репозиторий код, не соответствующий описанным правилам. Для того, чтобы проверить ваш код, выполните в корне проекта команду:

```flake8 ./```

Отсутствие какого-либо вывода в консоль означает, что ваш код прекрасен!
